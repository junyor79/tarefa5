import java.util.Calendar;

public class Primitivos{


    public static void main(String[] args) {
        
      
        String texto1 = ("Curso Mentorama - Modulo 5");
        System.out.println(texto1);

        int valor1 = 12;
        Integer valor2 = Integer.valueOf(valor1);
        System.out.println(valor2);

        Calendar datahoje = Calendar.getInstance();
        System.out.println(datahoje.getTime());

        char vetores[] = {'F', 'i', 'n', 'a', 'l', 'i', 'z', 'a', 'n', 'd', 'o',' ', 'T', 'a', 'r', 'e', 'f', 'a', ' ', '-', 'J', 'u', 'n', 'i', 'o', 'r'};
        String texto = String.valueOf(vetores);
        System.out.println(texto);

    }



}